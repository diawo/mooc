![alt tag](https://bitbucket.org/diawo/mooc/downloads/Scheme.png)

-------------------------------------------

Configuration System info:

Vagrant 2.0.2

Oracle VM VirtualBox 5.2.6

Ansible 2.4.3.0

Windows 10 Home Edition (host)

Ubuntu 16.04 LTS

----------------------------------------------
Initializing and testing Vagrant with VirtualBox

"Vagrant up"

-----------------------------------------------

Install Ansible with "pip"

-----------------------------------------------

Install Haproxy

-----------------------------------------------

"Run Ansible playbooks"

ansible-playbook -i hosts app_server.yml

ansible-playbook -i hosts haproxy.yml

ansible-playbook -i hosts backend_server.yml

------------------------------------------------

Install Load Balancer node with Haproxy

Load Balancer node:

- master-node 192.168.50.10

-------------------------------------------------

Install App node with Nginx:

Application node:

- slave-node 192.168.50.11

--------------------------------------------------
Install backend node with MongoDB:

Backend node:

- children-node 192.168.50.12

------------------------------------------------

Install keepalived to monitor haproxy process and lower the priority of the master node if it fails, 
leading to a failover of the other node.

------------------------------------------------

Virtual ip address:

192.168.50.87